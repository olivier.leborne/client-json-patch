package com.kyriba.example.clientjsonpatch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.function.Function;

@RestController
@RequestMapping("/client")
@Slf4j
public class ClientController {
    @Autowired
    private JsonModifier jsonModifier;
    private final WebClient webClient = WebClient.create("http://localhost:8080/server/resource");

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String triggerClient() {
        return doTriggerClient(jsonModifier::alterBodyPatch);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, path ="/merge")
    public String triggerClientMerge() {
        return doTriggerClient(jsonModifier::alterBodyMergePatch);
    }

    private String doTriggerClient(Function<String, String> bodyModifier) {
        log.info("Requesting /server/resource");
        return webClient.get().retrieve().bodyToMono(String.class)
                .flatMap(jsonBody -> {
                    log.info("JSON body received: {}", jsonBody);
                    String alteredJsonBody = bodyModifier.apply(jsonBody);
                    log.info("JSON body submitted: {}", alteredJsonBody);
                    return webClient.put().contentType(MediaType.APPLICATION_JSON)
                            .bodyValue(alteredJsonBody)
                            .retrieve()
                            .toBodilessEntity()
                            .map(responseEntity -> {
                                log.info("Response from server: {}", responseEntity.getStatusCode());
                                return alteredJsonBody;
                            });
                }).block();
    }


}
