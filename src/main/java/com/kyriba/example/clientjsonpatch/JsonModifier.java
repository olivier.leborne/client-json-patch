package com.kyriba.example.clientjsonpatch;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JsonModifier {

    @Autowired
    private ObjectMapper objectMapper;
    public String alterBodyPatch(String jsonBody) {
        try {
            JsonNode jsonNode = objectMapper.readTree(jsonBody);
            JsonPatch patch = objectMapper.readValue("""
                    [
                      { "op": "replace", "path": "/hello", "value": "mundo" },
                      { "op": "replace", "path": "/nested/sub", "value": "new value" },
                      { "op": "add", "path": "/nested/newSub", "value": { "subsub": "deep value" } },
                      { "op": "add", "path": "/nested/nestedArray/-", "value": "three" },
                      { "op": "remove", "path": "/foo" }
                    ]
                    """, JsonPatch.class);
            JsonNode alteredBody = patch.apply(jsonNode);
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(alteredBody);
        } catch (JsonProcessingException | JsonPatchException e) {
            throw new RuntimeException(e);
        }
    }

    public String alterBodyMergePatch(String jsonBody) {
        try {
            JsonNode jsonNode = objectMapper.readTree(jsonBody);
            JsonMergePatch mergePatch = objectMapper.readValue("""
                    {
                      "hello": "mundo",
                      "nested": {
                        "sub": "new value",
                        "newSub": { "subsub": "deep value" },
                        "nestedArray": [ "three" ]
                      },
                      "foo": null
                    }
                    """, JsonMergePatch.class);
            JsonNode alteredBody = mergePatch.apply(jsonNode);
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(alteredBody);
        } catch (JsonProcessingException | JsonPatchException e) {
            throw new RuntimeException(e);
        }
    }
}
