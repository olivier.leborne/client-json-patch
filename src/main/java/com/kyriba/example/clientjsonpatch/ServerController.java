package com.kyriba.example.clientjsonpatch;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/server")
@Slf4j
public class ServerController {

    @Autowired
    private ObjectMapper objectMapper;
    private static final String JSON_CORE = """
{
  "foo": "bar",
  "hello": "world",
  "nested": {
    "sub": "value",
    "nestedArray": [ "one", "two" ]
  }
}
""";
    public static final String DICTIONARY = "dictionary";
    public final List<String> dict;
    public final Random random = new Random();

    public ServerController() {
        try (InputStream resource = this.getClass().getClassLoader().getResourceAsStream(DICTIONARY)) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource));
            dict = bufferedReader.lines().collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping(value = "resource", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getResource() throws JsonProcessingException {
        log.info("Received request");
        Map<String, Object> object = objectMapper.readValue(JSON_CORE, Map.class);
        // add random properties
        for (int i = 0; i < 3; i++) {
            object.put("randomProperty" + randomWord(), randomWord());
            ((Map<String, Object>)object.get("nested")).put("randomProperty" + randomWord(), randomWord());
        }
        String jsonResponse = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        log.info("Returning JSON: {}", jsonResponse);
        return jsonResponse;
    }

    private String randomWord() {
        return dict.get(random.nextInt(dict.size()));
    }

    @PutMapping(value = "resource", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void putResource(@RequestBody String jsonBody) {
        log.info("Received JSON: {}", jsonBody);
    }
}
