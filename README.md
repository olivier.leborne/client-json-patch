# JSON Patch on REST API Client

This project demonstrates how to use [JSON Patch](https://github.com/java-json-tools/json-patch) inside a client for a REST API.

## Description

In this example, a REST API client will:
- load a JSON from a server using a GET method
- alter that JSON using JSON Patch semantics
- submit that altered payload to the server using a PUT method

The objective is to demonstrate how a client can alter a resource while maintaining compatibility while the model 
for that resource is evolved by adding extra properties.
This leverages the [Java JSON Tools JSON Patch](https://github.com/java-json-tools/json-patch) library. 

For the sake of simplicity, this example is in the form of a REST server.
* Some endpoints correspond to the REST API server mocked: `/server` and below
* Some endpoints are used to trigger the client behaviour: `/client` and below

The server inserts same random properties at the root of the object and nested inside the "nested" property object.
This demonstrate the capacity for the code to alter the JSON without dropping properties it is not aware of.

## Requirements

* JDK 17
* Gradle 8

## How to Run

In the folder where the project is located:

```shell
./gradlew bootRun
```

## How to trigger the example

To trigger the client, call `GET /client` on the local server listening on port 8080.
This will trigger the client and the behaviour can be observed in the logs, notably the altered value observed by the 
server.

```shell
curl http://localhost:8080/client
```

Modifications performed:
* value of "hello" replaced from "world" to "mundo"
* value of nested property "nested/sub" replaced from "value" to "new value"
* a new property named "newSub" is added in the "nested" property object and it is a complex object and not just a value
* an additional entry is inserted at the end of the nested array "nested/nestedArray"

Another endpoint if available: `GET /client/merge` to demonstrate how to perform a modification on a JSON using
the JSON Merge Patch syntax.

The same modifications are performed, except for array manipulations which are not possible with JSON Merge Patch
syntax. The array content is fully replaced with the new value.
